<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/', 'HomeController@index')->name('home');
Route::get('game/{id}', "GameController@index")->name('game');
Route::get('user/{id}', "UserController@index")->name('user');
Route::get('quizQuestions', "QuizQuestionsController@index")->name('quizQuestions');
Route::get('editQuestionModal/{id}', "QuizQuestionsController@editModal")->name('editQuestionModal');
Route::get('addQuestionModal', "QuizQuestionsController@addModal")->name('addQuestionModal');
Route::get('deleteQuestionModal/{id}', "QuizQuestionsController@deleteModal")->name('deleteQuestionModal');
Route::post('createOrUpdateQuestion', "QuizQuestionsController@createOrUpdate")->name('createOrUpdateQuestion');
Route::post('deleteQuestion', "QuizQuestionsController@delete")->name('deleteQuestion');
Route::post('unbanUser/{id}', "UserController@unbanUser")->name('unbanUser');
Route::post('banUser/{id}', "UserController@banUser")->name('banUser');
