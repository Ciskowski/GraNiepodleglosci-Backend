<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('token', 'Api\AuthController@getToken');
Route::post('auth', 'Api\AuthController@authenticate');
Route::get('game/{name}', 'Api\GameController@getInfo');
Route::get('ranking/{id}', 'Api\GameController@getTopPlayers');
Route::get('questions', 'Api\QuizController@getQuestions');
Route::post('register', 'Api\RegistrationController@registration');
Route::get('main', 'Api\RankingController@mainRanking');
Route::post('ranking/{id}', 'Api\RankingController@insertRanking');
