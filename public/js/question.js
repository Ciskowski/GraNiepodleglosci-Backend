var answerLetters = ['A', 'B', 'C', 'D'];

function getAnswerSelect(select, correct) {
    answerLetters.forEach(function(answer) {
        var option = document.createElement("option");
        option.value = answer;
        option.text = answer;
        option.className = "answer-select";
        if(correct == answer)
        {
            option.setAttribute("selected", "selected")
            setInputColor(correct);
        }
        select.append(option);
    })
}

function setInputColor(selectedVal)
{
    answerLetters.forEach(function(answer) {
        answer = answer.toLowerCase();
        document.getElementsByName(answer)[0].style.removeProperty('border-color');
    });
    selectedVal = selectedVal.toLowerCase();
    document.getElementsByName(selectedVal)[0].style.borderColor = "#42f462";
}