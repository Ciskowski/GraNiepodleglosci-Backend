<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">Add new question</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
      <form action="{{ route('createOrUpdateQuestion') }}" method="post">
      @csrf
        <input type="hidden"name="id">
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>Title: </b></div>
          <div><input type="text"  class="form-control"name="title"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>A: </b></div>
          <div><input type="text"  class="form-control" name="a"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>B: </b></div>
          <div><input type="text"  class="form-control" name="b"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>C: </b></div>
          <div><input type="text"  class="form-control" name="c"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>D: </b></div>
          <div><input type="text" class="form-control" name="d"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>Correct </b></div>
          <div><select name="correct" id="correctSelect" class="form-control" onchange="setInputColor(this.value)"></select></div>
        </div>
        <div class="form-group">
          <div><input type="submit" value="Add" class="btn btn-success"></div>
        </div>
      </form>
      <script>getAnswerSelect(document.querySelector("#correctSelect"), 'A')</script>
    </div>
  </div>
</div>