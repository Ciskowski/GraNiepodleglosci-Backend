@extends('layouts.app')

@section('content')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Games</div>

                <div class="card-body">
                    @foreach($model->games as $game)
                        <a href="{{route('game', ['id' => $game->id])}}">
                            <div class="game-box">
                                <h1>{{ $game->name }}</h1>
                            </div>
                        </a>
                    @endforeach
                </div>
                <br/>
                <hr/> 
                <h3 style="padding-left: 20px;">Edit</h3>
                <div class="row">
                    <div class="col-md-4" style="padding-left: 30px;">
                        <a href="{{ route('quizQuestions') }}"><button type="button" class="btn btn-info editButton">Edit quiz questions</button></a>
                    </div>
                </div>
                <hr/>  
                <h3 style="padding-left: 20px">Top players</h3>
                <table class="table table-striped">
                    <tr>
                        <th>Player</th>
                        <th>Average score</th>
                        <th>Average established time</th>
                    </tr>
                    @foreach($model->playersBestScore as $playerScore)
                    </tr>
                        <td><a href="{{ route('user', ['id' => $playerScore->player->id]) }}">{{ $playerScore->player->username }}</td>
                        <td>{{ round($playerScore->avgPoints, 2) }}</td>
                        <td>{{ round($playerScore->avgTime, 2) }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    setColor();

    function setColor() {
        const colorArr = ['#42a4f4', '#41f46b', '#f47941', '#f441e5', '#15f2cd'];

        var boxes = document.querySelectorAll('.game-box');
        let j = 0;
        for(i = 0; i < boxes.length; i++)
        {
            if(j > colorArr.length - 1)
                j = 0;
            boxes[i].style.backgroundColor = colorArr[i];

            j++;
        }
    }
</script>
@endsection
