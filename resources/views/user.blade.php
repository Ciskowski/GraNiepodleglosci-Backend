@extends('layouts.app')

@section('content')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div>Informations</div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-auto">Username:</div>
                        <div>{{ $model->name }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-auto">Registration date:</div>
                        <div>{{ $model->created_at }}</div>
                    </div>
                    <br/>
                    <br/>
                    @foreach($model->gamesStats as $stats)
                        <div class="row">
                            <div class="col-md-auto" style="font-size: 1.2em">
                                {{ $stats->game->name }}
                            </div>
                        </div>
                        <div style="margin-left:20px">
                              <div class="row">
                                <div class="col-md-auto">Highest place:</div>
                                <div>{{ $stats->ranking}}</div>
                              </div>  
                              <div class="row">
                                <div class="col-md-auto">Average time:</div>
                                <div>{{ $stats->averageTime or '-'}}</div>
                              </div>  
                              <div class="row">
                                <div class="col-md-auto">Amount of games:</div>
                                <div>{{ $stats->amountOfPlays or '-'}}</div>
                              </div>
                              <div class="row">
                                <div class="col-md-auto">Amount of games in last week:</div>
                                <div>{{ $stats->amountOfPlaysInPeriod or '-'}}</div>
                              </div>    
                        </div>
                    @endforeach
                    <br/>
                    <br/>
                    <div class = "row">
                        <div class="col-md-auto">
                            <a href="/"><button class="btn btn-secondary">Go back</button></a>
                        </div>
                         <div class="col-md-auto">
                            @if($model->isBanned)
                                <form action="{{ route('unbanUser', ['id'=>$model->id, 'returnUrl'=>Request::path()]) }}" method="post">
                                    @csrf
                                    <input type="submit" class="btn btn-success" value="Unban">
                                </form>
                            @else
                                <form action="{{ route('banUser', ['id'=>$model->id, 'returnUrl'=>Request::path()]) }}" method="post">
                                    @csrf
                                    <input type="submit" class="btn btn-danger" value="Ban">
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
