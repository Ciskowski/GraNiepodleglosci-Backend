@extends('layouts.app')

@section('content')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
             @endif
            <div class="card">
                <div class="card-header">
                    <div>Game <i>{{ $model->name }}</i></div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-auto">Amount of games:</div>
                        <div class="col-md-10">{{ $model->stats->amountOfGames }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-auto">Amount of games in this week:</div>
                        <div class="col-md-9">{{ $model->stats->amountInPeriod }}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-auto">Average time of playing:</div>
                        <div class="col-md-9">{{ $model->stats->averageTime or 0}}</div>
                    </div>
                </div>
            </div>
            <br/><br/>
            <h3>Top 10 players</h3>
            @php $i = 1 @endphp
            <table class="table table-striped">
                <tr>
                    <th>Place</th>
                    <th>Name</th>
                    <th>Points</th>
                    <th>Time</th>
                    <th>Reached at</th>
                    <th></th>
                </tr>
                @foreach($model->stats->topUserRanking as $user)
                    <tr>
                        <td>{{ $i }}</td>
                        <td><a href="{{ route('user', ['id'=>$user->id]) }}">{{ $user->username }}</a></td>
                        <td>{{ $user->points }}</td>
                        <td>{{ $user->established_time }}</td>
                        <td>{{ $user->created_at }}</td>
                    @if($user->isBanned)
                        <td>
                            <form action="{{ route('unbanUser', ['id'=>$user->id, 'returnUrl'=>Request::path()]) }}" method="post">
                                @csrf
                                <input type="submit" class="btn btn-success" value="Unban">
                            </form>
                        </td>
                    @else
                        <td>
                            <form action="{{ route('banUser', ['id'=>$user->id, 'returnUrl'=>Request::path()]) }}" method="post">
                                @csrf
                                <input type="submit" class="btn btn-danger" value="Ban">
                            </form>
                        </td>
                    @endif
                    </tr>
                    @php $i++ @endphp
                @endforeach
            </table>
            <div>
                <a href="{{route('home')}}"><button class="btn btn-secondary" style="margin-top: 5px">Go back</button></a>
            </div>
        </div>
    </div>
</div>
@endsection
