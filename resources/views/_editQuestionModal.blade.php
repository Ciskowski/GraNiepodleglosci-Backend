<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">Edit question</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
      <form action="{{ route('createOrUpdateQuestion') }}" method="post">
      @csrf
        <input type="hidden" value="{{ $model->id }}" name="id">
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>Title: </b></div>
          <div><input type="text"  class="form-control" value="{{ $model->title }}" name="title"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>A: </b></div>
          <div><input type="text"  class="form-control" value="{{ $model->a }}" name="a"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>B: </b></div>
          <div><input type="text"  class="form-control" value="{{ $model->b }}" name="b"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>C: </b></div>
          <div><input type="text"  class="form-control" value="{{ $model->c }}" name="c"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>D: </b></div>
          <div><input type="text" class="form-control" value="{{ $model->d }}" name="d"></div>
        </div>
        <div class="row form-group">
          <div class="col-md-2 input-text"><b>Correct </b></div>
          <div><select name="correct" id="correctSelect" class="form-control" value="{{ $model->correct }}" onchange="setInputColor(this.value)"></select></div>
        </div>
        <div class="form-group">
          <div><input type="submit" value="Save" class="btn btn-primary"></div>
        </div>
      </form>
      <script>getAnswerSelect(document.querySelector("#correctSelect"), '{{ $model->correct }}')</script>
    </div>
  </div>
</div>