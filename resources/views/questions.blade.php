@extends('layouts.app')

@section('content')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<script>
    function setCorrectAnswer(question, correctAnswer)
    {
        var item = document.getElementById('item' + question + "_" + correctAnswer);
        item.style.borderColor = "#42f462";
        item.style.backgroundColor = "rgba(122, 249, 145, 0.3)";
    }
</script>
<script src="{{ asset('js/question.js') }}" defer></script>

<div class="modal fade" id="modal_data" role="dialog">
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
             @endif
            <div class="card">
                <div class="card-header">
                    <div style="float: left">Edit quiz questions</div>
                    <button class="btn btn-success" style="float: right" onclick="get_modal('{{ route('addQuestionModal') }}')">Add new question</button>
                </div>

                <div class="card-body">

                    <div class="question-table">
                    @foreach($model->questions as $question)
                        <div class="row">
                            <div class="question-title col-md-2">{{ $question->title }}</div>
                            <div class="answers-box col-md-8">
                                <div id="item{{ $question->id }}_A" class="question-answer"><b>A</b>. {{ $question->A }}</div>
                                <div id="item{{ $question->id }}_B" class="question-answer"><b>B</b>. {{ $question->B }}</div>
                                <div id="item{{ $question->id }}_C" class="question-answer"><b>C</b>. {{ $question->C }}</div>
                                <div id="item{{ $question->id }}_D" class="question-answer"><b>D</b>. {{ $question->D }}</div>
                                <script>setCorrectAnswer({{ $question->id}}, '{{ $question->correct }}')</script>
                            </div>
                            <div class="col-md-2 question-panel">
                                <button class="btn btn-primary" onclick="get_modal('{{ route('editQuestionModal', [ 'id'=> $question->id]) }}')">Edit</button>
                                <button class="btn btn-danger" onclick="get_modal('{{ route('deleteQuestionModal', [ 'id'=> $question->id]) }}')">Delete</button>
                            </div>
                        </div>
                        <hr/>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"> 
    function get_modal(route){
        $.ajax({
            type    : 'GET', 
            url     : route,
            cache   : false,
            success : function(data){ 
               if(data){
                    $('#modal_data').html(data);
                    $('#modal_data').modal();
               }
            }
        });
    }
</script>
@endsection
