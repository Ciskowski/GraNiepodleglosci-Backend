<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title">Delete question</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
      <form action="{{ route('deleteQuestion') }}" method="post">
      @csrf
        <input type="hidden" name="id" value="{{ $model }}">
        <div><h3>Are sure you want to delete this question?</h3></div>
        <div class="row">
            <div style="margin: 0 20px"><input type="submit" value="Sure!" class="btn btn-danger"></div>
            <div><input type="button" value="Nope" class="btn btn-light" data-dismiss="modal"></div>
        </div>
      </form>
    </div>
  </div>
</div>