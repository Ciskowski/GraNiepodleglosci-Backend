<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'quiz_questions';

    public $timestamps = false;

    protected $fillable = [
        'title', 'A', 'B', 'C', 'D', 'correct'
    ];
}

?>