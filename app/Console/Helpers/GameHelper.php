<?php
    namespace App\Console\Helpers;

    use DB;
    use App\Http\ViewModels;

    class GameHelper
    {
        public static function getAllGames()
        {
            return DB::table('games')->get();
        }

        public static function getGame($id)
        {
            return DB::table('games')->where('id', $id)->first();
        }

        public static function getGameByName($name)
        {
            return DB::table('games')->where('name', $name)->first();
        }

        public static function getGameStats($game)
        {
            $topPlayersAmount = 10;
            $statisticDaysPeriod = 7;

            $returnModel = new ViewModels\GameStatsViewModel;
            $returnModel->averageTime = StatsHelper::getAverageGameTime($game->id);
            $returnModel->amountOfGames = StatsHelper::getAmountOfPlays($game->id);
            $returnModel->amountInPeriod = StatsHelper::getAmountOfPlays($game->id, null, $statisticDaysPeriod);
            $returnModel->topUserRanking = RankingHelper::getTopPlayers($game->id, $topPlayersAmount);

            return $returnModel;
        }
    }
?>