<?php
    namespace App\Console\Helpers;

    use DB;
    use Carbon\Carbon;

    class StatsHelper
    {
        public static function getAmountOfPlays($gameId, $playerId = null, $dayPeriod = null)
        {
            $query = DB::table('rankings')
                    ->where("game_id", $gameId);

            if($playerId != null) {
                $query->where('user_id', $playerId);
            }

            if($dayPeriod != null)
            {
                $query->whereDate('created_at', '>', Carbon::now()->subDays($dayPeriod));
            }

            return $query->count();
        }

        public static function getAverageGameTime($gameId, $playerId = null)
        {
            $query = DB::table('rankings')
                    ->where("game_id", $gameId);

            if($playerId != null)
            {
                $query = $query->where("user_id", $playerId);
            }

            return $query->avg('established_time');
        }
    }
?>