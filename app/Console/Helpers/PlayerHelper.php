<?php
    namespace App\Console\Helpers;
    
    use DB;
    use Exception;
    use App\Http\ViewModels;
    use App;

    class PlayerHelper
    {
        public static function getAllPlayers()
        {
            return DB::table('players')
                    ->get();
        }

        public static function getPlayer($id)
        {
            return DB::table('players')
                    ->where("id", $id)
                    ->first();
        }

        public static function findPlayer($username)
        {
            return $player = DB::table('players')
                    ->where("username", $username)
                    ->first();
        }

        public static function getPlayerStats($player, $game)
        {
            $statisticDaysPeriod = 7;

            $returnModel = new ViewModels\PlayerGameStatsViewModel();
            $returnModel->game = $game;
            $returnModel->averageTime = StatsHelper::getAverageGameTime($game->id, $player->id);
            $returnModel->amountOfPlays = StatsHelper::getAmountOfPlays($game->id, $player->id);
            $returnModel->amountOfPlaysInPeriod = StatsHelper::getAmountOfPlays($game->id, $player->id, $statisticDaysPeriod);
            $returnModel->ranking = RankingHelper::getPlayerPosition($game->id, $player->id);

            return $returnModel;
        }

        public static function getPlayerAllGamesScore($player, $games)
        {
            $avgPoints = 0;
            $avgTime = 0;
            foreach($games as $game)
            {
                $bestScore = RankingHelper::getPlayerBestScore($game->id, $player->id);
                if($bestScore == null)
                {
                    continue;
                }
                $avgPoints += $bestScore->points;
                $avgTime += $bestScore->established_time;
            }

            $returnModel = new ViewModels\SummaryScoreViewModel();
            $returnModel->player = new App\Player();
            $returnModel->player->id = $player->id;
            $returnModel->player->username = $player->username;
            $returnModel->avgPoints = $avgPoints / count($games);
            $returnModel->avgTime = $avgTime / count($games);

            return $returnModel;
        }
    }
?>