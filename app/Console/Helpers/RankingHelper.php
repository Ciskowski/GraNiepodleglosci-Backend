<?php
    namespace App\Console\Helpers;
    
    use DB;

    class RankingHelper
    {
        public static function getAllPlayersSorted($gameId) 
        {
            return DB::table('rankings')
                    ->join('players', 'rankings.user_id', '=', 'players.id')
                    ->where('game_id', $gameId)
                    ->orderBy('points', 'desc')
                    ->orderBy('established_time')
                    ->orderBy('created_at')
                    ->get(['rankings.*', 'players.id', 'players.username', 'players.isBanned']);
        }

        public static function getTopPlayers($gameId, $topPlayersAmount) 
        {
            return RankingHelper::getAllPlayersSorted($gameId)
                        ->take($topPlayersAmount);
        }

        public static function getPlayerBestScore($gameId, $playerId)
        {
            return DB::table('rankings')
                ->where('game_id', $gameId)
                ->where('user_id', $playerId)
                ->orderBy('points', 'desc')
                ->orderBy('established_time')
                ->orderBy('created_at')
                ->first();
        }

        public static function getPlayerPosition($gameId, $playerId)
        {
            $players = RankingHelper::getAllPlayersSorted($gameId);
            $playerPosition = 1;

            foreach($players as $player)
            {
                if($player->user_id == $playerId)
                {
                    return $playerPosition;
                }

                $playerPosition++;
            }

            return '-';
        }
    }
?>