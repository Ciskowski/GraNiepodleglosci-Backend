<?php
    namespace App\Console\Helpers;

    use DB;

    class QuestionHelper
    {
        public static function getAllQuestions()
        {
            return DB::table('quiz_questions')
                    ->get();
        }

        public static function getQuestion($id)
        {
            return DB::table('quiz_questions')
                    ->where('id', $id)
                    ->first();
        }
    }
?>