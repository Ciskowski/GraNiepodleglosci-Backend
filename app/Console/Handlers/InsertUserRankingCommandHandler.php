<?php
    namespace App\Console\Handlers;

    use App;

    class InsertUserRankingCommandHandler
    {
        public function handle($command)
        {
            $question = App\Ranking::insert(
                array(
                    'game_id' => $command->gameId,
                    'user_id' => $command->playerId,   
                    'points' => $command->points,   
                    'established_time' => $command->establishedTime,  
                    'created_at' => date('Y-m-d')
                )
            );
        }
    }
?>