<?php
    namespace App\Console\Handlers;

    use App;

    class DeleteQuestionCommandHandler
    {
        public function handle($command)
        {
            $question = App\Question::where('id', $command->id)->first();
            $question->delete();
        }
    }
?>