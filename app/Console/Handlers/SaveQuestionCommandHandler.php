<?php
    namespace App\Console\Handlers;

    use App;

    class SaveQuestionCommandHandler
    {
        public function handle($command)
        {
            $question = App\Question::updateOrCreate(
                ['id' => $command->id ],
                [
                    'title' => $command->title,
                    'A' => $command->a,   
                    'B' => $command->b,   
                    'C' => $command->c,  
                    'D' => $command->d,
                    'correct' => $command->correct   
                ]
            );
        }
    }
?>