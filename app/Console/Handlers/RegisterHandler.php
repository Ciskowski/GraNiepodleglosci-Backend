<?php
    namespace App\Console\Handlers;

    use App;
    use DB;

    class RegisterHandler
    {
        public function handle($command)
        {
            $date = date("Y-m-d H:i:s");

            $insert = DB::table('players')->insertGetId([
                                        'username' => $command->username,
                                        'password' => $command->password,
                                        'created_at' => $date,
            ]);

            if (!$insert) {
                throw new Exception ("Some error while adding new player");
            }

            return true;
        }
    }
?>