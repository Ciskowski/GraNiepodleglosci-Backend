<?php
  
    namespace App\Console\Handlers;

    use DB;
    
    class BanCommandHandler 
    {
        public function handle($command)
        {
            $update = DB::table('players')
                    ->where('id', $command->userId)
                    ->update([ 'isBanned' => $command->banUser ]);
        }
    }
?>