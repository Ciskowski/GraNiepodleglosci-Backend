<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BanCommand
{
    public $userId;

    public $banUser = true;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }
}
