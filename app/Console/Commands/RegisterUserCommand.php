<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RegisterUserCommand 
{
    public $username;

    public $password; 
   
    public function __construct($username, $password)
    { 
        $this->username = $username;
        $this->password = $password;
    }

}
