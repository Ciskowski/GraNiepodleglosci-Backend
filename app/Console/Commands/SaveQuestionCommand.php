<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App;

class SaveQuestionCommand
{   
    public $id;

    public $title;

    public $a;

    public $b;

    public $c;

    public $d;

    public $correct;

    public function __construct($question)
    {
        if($question->id != null) 
        {
            $this->id = $question->id;
        }

        $this->title = $question->title;
        $this->a = $question->a;
        $this->b = $question->b;
        $this->c = $question->c;
        $this->d = $question->d;
        $this->correct = $question->correct;
    }
}
