<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App;

class InsertUserRankingCommand
{   
    public $gameId;

    public $playerId;

    public $points;

    public $establishedTime;

    public function __construct($gameId, $player)
    {
        $this->gameId = $gameId;
        $this->playerId = $player->id;
        $this->points = $player->points;
        $this->establishedTime = $player->establishedTime;
    }
}
