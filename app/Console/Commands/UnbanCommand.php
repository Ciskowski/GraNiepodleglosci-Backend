<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UnbanCommand 
{
    public $userId;

    public $banUser = false;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }
}
