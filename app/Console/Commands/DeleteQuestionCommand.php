<?php
    namespace App\Console\Commands;

    class DeleteQuestionCommand
    {
        public $id;

        public function __construct($id)
        {
            $this->id = $id;
        }
    }
?>