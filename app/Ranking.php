<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ranking extends Model
{
    protected $table = 'rankings';

    protected $fillable = [
        'game_id', 'user_id', 'points', 'created_at', 'established_time'
    ];
}

?>