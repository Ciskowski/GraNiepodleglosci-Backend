<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Console\Helpers;
use App\Console\Commands;
use App\Console\Handlers;
use Illuminate\Support\Facades\Hash;
use Exception;

class RegistrationController extends \App\Http\Controllers\Controller
{

   public function registration(Request $request)
   {
        $bodyContent = (object)$request->all();
        $username = $bodyContent->username;
        $password = $bodyContent->password;
        $confirmPassword = $bodyContent->confirmPassword;

        $response = new ApiResponse();
        
        try {
            $isPlayerInDB = Helpers\PlayerHelper::findPlayer($username);

            if ($isPlayerInDB)
                throw new Exception("Ta nazwa gracza jest już zajęta");

            $this->validPassword($password, $confirmPassword);
            $password = $this->hashPassword($password);

            $command = new Commands\RegisterUserCommand($username, $password);
            $handler = new Handlers\RegisterHandler();
    
            $handler->handle($command);

            $response->status = 200;
            $response->data = array("message" => "Dodano nowego gracza");
        }
        catch(Exception $e) {
            $response->status = 500; 
            $response->data = array("message" => $e->getMessage());
        }

        return response()->json($response);
    }
   
   
   private function validPassword($password, $confirmPassword) {
        if (strlen($password) < 6) 
            throw new Exception ("Hasło jest zbyt krótkie. Minimalna długość: 6 znaków");
        if ($password != $confirmPassword) 
            throw new Exception ("Hasła nie są takie same");
    }
    
    private function hashPassword($password) {
        return Hash::make($password);
    }

}


