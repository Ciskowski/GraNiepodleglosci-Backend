<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Console\Helpers;

class GameController extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getInfo($name)
    {
        $data = Helpers\GameHelper::getGameByName($name);

        $response = new ApiResponse();
        if (empty($data))
            $response->status = 404;

        $response->data = $data;

        return response()->json($response);
    }

    public function getTopPlayers($id)
    {
        $topPlayersAmount = 10;
        $game = Helpers\GameHelper::getGame($id);
        $response = new ApiResponse();
        if (!empty($game)) {
            $data = Helpers\RankingHelper::getTopPlayers($id, $topPlayersAmount);
            $response->data = $data;
        }
        else {
            $response->status = 404;
        }

        return response()->json($response);
    }
}
