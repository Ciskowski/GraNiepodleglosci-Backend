<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Console\Helpers;

class QuizController extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getQuestions()
    {
        $data = Helpers\QuestionHelper::getAllQuestions();

        $response = new ApiResponse();
        if (empty($data))
            $response->status = 404;

        $response->data = $data;

        return response()->json($response);
    }
}
