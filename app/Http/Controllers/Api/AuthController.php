<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Console\Helpers;
use App\Player;
use Exception;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use Response;
use \Laravel\Passport\Http\Controllers\AccessTokenController as 
ATC;


class AuthController extends ATC
{
    public function authenticate(Request $request)
    {
        $client = DB::table('oauth_clients')->where('id', 2)->first();

        $request->request->add([
            'grant_type' => 'password',
            'username' => $request->username,
            'password' => $request->password,
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'scope' => '*'
        ]);

        $proxy = Request::create(
            'api/token',
            'POST'
        );

        return \Route::dispatch($proxy);
    }

    public function getToken(ServerRequestInterface $request)
    {
        $response = new ApiResponse();

        try {
            $username = $request->getParsedBody()['username'];

            $user = Player::where('username', '=', $username)->first();

            if (@$user->isBanned)
                throw new Exception("Użytkownik został zbanowany");

            $tokenResponse = parent::issueToken($request);

            $content = $tokenResponse->getContent();
            $data = json_decode($content, true);

            if(isset($data["error"]))
                throw new OAuthServerException('Błędne dane logowania', 6, 'invalid_credentials', 401);

            $response->data = array('accessToken' => $data['access_token']);
            $response->data += array('tokenType' => $data['token_type']);
            $response->data += array('userId' => $user->id);
            $response->data += array('username' => $user->username);
            $response->status = 200;
        
            return response()->json($response);
        }
        catch (ModelNotFoundException $e) {
            $response->data = array("message" => "Nie odnaleziono gracza");
            $response->status = 404;

            return response()->json($response);
        }
        catch (OAuthServerException $e) {
            $response->data = array("message" => $e->getMessage());
            $response->status = 401;

            return response()->json($response);
        }
        catch (Exception $e) {
            $response->data = array("message" => $e->getMessage());
            $response->status = 500;

            return response()->json($response);
        }
        
    }
}
