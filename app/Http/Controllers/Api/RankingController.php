<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Console\Helpers;
use App\Http\ViewModels;
use App\Console\Commands;
use App\Console\Handlers;

class RankingController extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function insertRanking($id, Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required',
            'points' => 'required',
            'establishedTime' => 'required',
        ]);

        $model = (object)$request->all();
        if (Helpers\PlayerHelper::getPlayer($model->id) == null)
            return response("Nie odnaleziono gracza", 404);
        $command = new Commands\InsertUserRankingCommand($id, $model);
        $handler = new Handlers\InsertUserRankingCommandHandler();
        $handler->handle($command);

        $response = new ApiResponse();
        $response->status = 200;
        
        return response()->json($response);
    }

    public function mainRanking() 
    {
        $topPlayersAmount = 10;

        $model = (object)[];
        $games = Helpers\GameHelper::getAllGames();
        $players = Helpers\PlayerHelper::getAllPlayers();

        $playersBestScore = array();
        foreach($players as $player)
        {
            $playersBestScore[] = Helpers\PlayerHelper::getPlayerAllGamesScore($player, $games);
        }

        $playersBestScore = $this->sortTopPlayerArray($playersBestScore, $topPlayersAmount);

        $model = $playersBestScore;

        return response()->json($model);
    }

    private function sortTopPlayerArray($playerArray, $amount)
    {
        usort($playerArray, function($first, $second) {
            return $first->avgPoints < $second->avgPoints;
        });

        usort($playerArray, function($first, $second) {
            return $first->avgTime < $second->avgTime;
        });

        return array_slice($playerArray, 0, $amount);
    }
}
