<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Console\Helpers;
use App\Http\ViewModels;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topPlayersAmount = 10;

        $model = new ViewModels\HomeViewModel();
        $games = Helpers\GameHelper::getAllGames();
        $players = Helpers\PlayerHelper::getAllPlayers();

        $playersBestScore = array();
        foreach($players as $player)
        {
            $playersBestScore[] = Helpers\PlayerHelper::getPlayerAllGamesScore($player, $games);
        }

        $playersBestScore = $this->sortTopPlayerArray($playersBestScore, $topPlayersAmount);

        $model->games = $games;
        $model->playersBestScore = $playersBestScore;

        return view('home')->with('model', $model);
    }

    private function sortTopPlayerArray($playerArray, $amount)
    {
        usort($playerArray, function($first, $second) {
            return $first->avgPoints < $second->avgPoints;
        });

        usort($playerArray, function($first, $second) {
            return $first->avgTime < $second->avgTime;
        });

        return array_slice($playerArray, 0, $amount);
    }
}
