<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Console\Helpers;
use App\Http\ViewModels;
use App\Console\Commands;
use App\Console\Handlers;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $player = Helpers\PlayerHelper::getPlayer($id);
        $games = Helpers\GameHelper::getAllGames();
        $model = new ViewModels\PlayerViewModel();
        $gamesStats = array();

        if($player != null && $games != null)
        {
            foreach($games as $game)
            {
                $gamesStats[] = Helpers\PlayerHelper::getPlayerStats($player, $game);
            }
    
            $model->id = $player->id;
            $model->name = $player->username;
            $model->isBanned = $player->isBanned;
            $model->created_at = $player->created_at;
            $model->gamesStats = $gamesStats;
        } else {
            return abort(404);
        }

        return view("user")->with('model', $model);
    }

    public function banUser($id)
    {
        $command = new Commands\BanCommand($id);
        $handler = new Handlers\BanCommandHandler();
        $returnUrl = Input::get('returnUrl');

        $handler->handle($command);

        return redirect($returnUrl)->with('success', 'User ban status has been changed');        
    }

    public function unbanUser($id)
    {
        $command = new Commands\UnbanCommand($id);
        $handler = new Handlers\BanCommandHandler();
        $returnUrl = Input::get('returnUrl');

        $handler->handle($command);

        return redirect($returnUrl)->with('success', 'User ban status has been changed');        
    }
}
