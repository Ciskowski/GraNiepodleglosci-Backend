<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\ViewModels;
use App\Console\Helpers;
use App\Console\Commands;
use App\Console\Handlers;

class QuizQuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $model = new ViewModels\QuestionsViewModel();
        $model->questions = Helpers\QuestionHelper::getAllQuestions();

        return view('questions')->with('model', $model);
    }

    public function editModal($id)
    {
        $model = new ViewModels\EditQuestionViewModel();
        $question = Helpers\QuestionHelper::getQuestion($id);

        $model->id = $question->id;
        $model->title = $question->title;
        $model->a = $question->A;
        $model->b = $question->B;
        $model->c = $question->C;
        $model->d = $question->D;
        $model->correct = $question->correct;
        return view('_editQuestionModal')->with('model', $model);
    }

    public function addModal()
    {
        return view('_addQuestionModal');
    }

    public function deleteModal($id)
    {
        return view('_deleteQuestionModal')->with('model', $id);
    }

    public function createOrUpdate(Request $request)
    {
        $returnMessage;
        $validatedData = $request->validate([
            'title' => 'required',
            'a' => 'required',
            'b' => 'required',
            'c' => 'required',
            'd' => 'required',
            'correct' => [function($attribute, $val, $fail){
                $this->isCheckLetterCorrect($attribute, $val, $fail);
            }]
        ]);

        $model = (object)$request->all();
        $command = new Commands\SaveQuestionCommand($model);
        $handler = new Handlers\SaveQuestionCommandHandler();
        $handler->handle($command);

        if($this->isQuestionCreated($model))
            $returnMessage = "Question successfully added";
        else
            $returnMessage = "Question successfully edited";

        return redirect('quizQuestions')->with('success', $returnMessage);
    }

    public function delete(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required'
        ]);

        $model = (object)$request->all();
        $command = new Commands\DeleteQuestionCommand($model->id);
        $handler = new Handlers\DeleteQuestionCommandHandler();
        $handler->handle($command);

        return redirect('quizQuestions')->with('success', 'Question successfully deleted');
    }

    private function isQuestionCreated($model)
    {
        return $model->id == null;
    }

    private function isCheckLetterCorrect($attr, $val, $fail)
    {
        if($val != 'A' && $val != 'B' && $val != 'C' && $val != 'D')
        {
            $fail("$val letter is not correct answer letter");
        }
    }
}
