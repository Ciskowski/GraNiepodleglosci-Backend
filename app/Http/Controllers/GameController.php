<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Console\Helpers;
use App\Http\ViewModels;

class GameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $game = Helpers\GameHelper::getGame($id);
        $model = new ViewModels\GameViewModel();

        if($game != null) {
            $model->id = $game->id;
            $model->name = $game->name;
            $model->stats = Helpers\GameHelper::getGameStats($game);
        }
        else {
            return abort(404);
        }

        return view('game')->with('model', $model);
    }
}
