<?php
    
    namespace App\Http\ViewModels;

    class GameStatsViewModel
    {
        public $averageTime;

        public $amountOfGames;

        public $amountInPeriod;

        public $topUserRanking;
    }
?>