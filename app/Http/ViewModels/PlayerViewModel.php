<?php
    namespace App\Http\ViewModels;

    class PlayerViewModel
    {
        public $id;

        public $name;

        public $created_at;

        public $gamesStats;

        public $isBanned;
    }
?>