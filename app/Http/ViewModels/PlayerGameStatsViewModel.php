<?php
    namespace App\Http\ViewModels;

    class PlayerGameStatsViewModel
    {
        public $game;

        public $averageTime;

        public $amountOfPlays;

        public $amountOfPlaysInPeriod;

        public $ranking;

    }
?>